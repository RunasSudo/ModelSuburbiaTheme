# Model Suburbia Reddit theme

An extensible, configurable and modular Sass/CSS theme for Reddit, used by /r/ModelSuburbiaCouncil, retaining the minimalist character of the default Reddit theme, with a splash of the indulgence found in themes like Minimaluminiumalism and Naut.

## Licence

This theme is licensed under the GNU Affero General Public License version 3 or later, with the **additional permission** that you may convey a covered work in object code form without also conveying the machine-readable Corresponding Source and without the copy of the GNU GPL normally required by section 4, provided that you comply with the other terms of sections 4 and 5.

This means that you may directly use the compiled output from *main.scss* on Reddit without any further immediate obligations.
